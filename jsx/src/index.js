// Import the React and ReactDom Libraries
import React from 'react';
import ReactDOM from 'react-dom';


//create a react component
const App = function() {
    const buttonText = { text:"Hi! there. Submit your Form Here", label:"Enter name:"};
    return (
    <div>Hi there!
     <form >
         <label className="label" htmlFor="name">{buttonText.label}</label>
         <input id="name" name="name" type="text" /><br />
         <button style={{backgroundColor:'Blue',color:'white',borderColor:'Blue',borderRadius:'5px',padding:'5px'}}>{buttonText.text}</button>
     </form>
     </div>
    );
};

// take the react component and show it on the screen 
ReactDOM.render(
    <App />,
    document.querySelector('#root')
);
