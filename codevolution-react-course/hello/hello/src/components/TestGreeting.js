import React, { Component } from 'react'

class TestGreeting extends Component {
    constructor(props) {
      super(props)
    
      this.state = {
         isLoggedIn : true,
         message: 'Hello '
      }
      this.doGreeting = this.doGreeting.bind(this)
    }
    userGreet(){
        return <div>Hello Sonia</div>;
    }
    guestGreet(){
        return <div>Hello Guest</div>;
    }

    doGreeting(){
        if(this.state.isLoggedIn){
            return alert(<userGreet />);
        }else{
            return alert(<guestGreet />);
        }
    }
    render() {
        <this.userGreet />
        if(this.state.isLoggedIn){
            return <userGreet />
        }else{
            return <guestGreet />
        }
        }
    }
    
    export default TestGreeting