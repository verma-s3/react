import React, { Component } from 'react'

class EventBind extends Component {
    constructor(props) {
      super(props)
    
      this.state = {
         message: 'Hello'
      }
    //   this.clickhandler = this.clickhandler.bind(this); 
      {/* this method is king of first method but it does not rerender over and over again web component is called */}
    }

    // clickhandler(){
    //     this.setState({
    //         message: 'Good bye!'
    //     })
    // }

    clickhandler = ()=>{
        this.setState({
            message: 'Good bye!'
        })
    }
  render() {
    return (
        <div>
            EventBind <br />{this.state.message}<br /><br />
            {/* <button onClick={this.clickhandler.bind(this)}>Clickme</button> */}  {/*//event binding method 1 */}
            {/* <button onClick={()=>this.clickhandler()}>Clickme</button> */}  {/* event bindign method 2 */}
            <button onClick={this.clickhandler}>Clickme</button> {/* event bindign method 3 */}
        </div>
    )
  }
}

export default EventBind