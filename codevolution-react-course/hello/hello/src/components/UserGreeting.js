import React, { Component } from 'react'

class UserGreeting extends Component {
    constructor(props) {
      super(props)
    
      this.state = {
         isLoggedIn: false
      }
    }
    // fist part
//   render() {
//     if(this.state.isLoggedIn){
//         return <div>Welcome Sonia</div>
//     }else{
//         return <div>Welcome guest</div>
//     }  
//     // return (
//     //   <div>
          
        
//     //     </div>
//     // )
//   }
// Secnonds part
        // render(){
        //     let message
        //     if(this.state.isLoggedIn){
        //         message = <div>Hello Sonia</div>
        //     }else{
        //         message = <div>Hello Guest</div>
        //     }
        //     return(
        //         <>
        //             {message}
        //         </>
        //     )
        // }

//third way -- trinart operator
        // render(){
        //     return (
        //         this.state.isLoggedIn ?
        //         <div>WelcomeSonia</div> :
        //         <div>Welcome Guest</div>
        //     )
        // }

//fourth part --shortcircut method
        render(){
            return this.state.isLoggedIn && <div>Hello Sonia</div>
        }

}

export default UserGreeting