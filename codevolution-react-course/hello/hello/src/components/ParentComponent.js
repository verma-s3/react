import React, { Component } from 'react'
import ChildComponent from './ChildComponent'

class ParentComponent extends Component {
    constructor(props) {
      super(props)
    
      this.state = {
         parentName: 'Parent'
      }
      //binding state
        this.greetParent = this.greetParent.bind(this)
    }

    greetParent(childName) {
        // this.setState({})
        alert(`Hello ${this.state.parentName} from ${childName}`);
    }
  render() {
    return (
      <div>
        ParentComponent
        <ChildComponent greethandler = {this.greetParent}/>
        </div>
    )
  }
}

export default ParentComponent