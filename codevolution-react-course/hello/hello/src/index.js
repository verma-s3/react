import React, {Component} from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import FunctionClick from './components/FunctionClick';
import EventBind from './components/EventBind';
import ParentComponent from './components/ParentComponent';
import UserGreeting from './components/UserGreeting';
import TestGreeting from './components/TestGreeting';

class Element extends Component {
  render() {
    return (
      <div>
        {/* First Page Here - learn React here
        <FunctionClick />
        <EventBind /> */}
        {/* <ParentComponent /> */}
        {/* <UserGreeting /> */}
        <TestGreeting />
      </div>
    )
  }
}


const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
    <Element />
);