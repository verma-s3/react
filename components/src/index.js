// import t libraries react and reactdom
import React from "react";
import ReactDOM from "react-dom";
import faker from "faker";
import CommentDetail from "./CommentDetail";
import ApprovalCard from "./ApprovalCard";

//fro refreshing code faster
if (module.hot) {
  module.hot.accept();
}

// create a react component
const App = function () {
  return (
    <div className="ui container comments ">
      {/* fro javascript jsx reference */}
      <ApprovalCard>
        <CommentDetail
          author={faker.name.firstName()}
          timedate="Today at 6:23 PM"
          text={faker.hacker.phrase()}
          image={faker.image.avatar()}
        />
      </ApprovalCard>
      <ApprovalCard>
        <CommentDetail
          author={faker.name.firstName()}
          timedate="Today at 2.30 PM"
          text={faker.hacker.phrase()}
          image={faker.image.avatar()}
        />
      </ApprovalCard>
      <ApprovalCard>
        <CommentDetail
          author={faker.name.firstName()}
          timedate="Today at 11.00 AM"
          text={faker.hacker.phrase()}
          image={faker.image.avatar()}
        />
      </ApprovalCard>
      <ApprovalCard>
        <CommentDetail
          author={faker.name.firstName()}
          timedate="Yesterday at 2.23 PM"
          text={faker.hacker.phrase()}
          image={faker.image.avatar()}
        />
      </ApprovalCard>
      <ApprovalCard>
        <CommentDetail
          author={faker.name.firstName()}
          timedate="2 Days ago at 8.12 AM"
          text={faker.hacker.phrase()}
          image={faker.image.avatar()}
        />
      </ApprovalCard>
      {/* <div className="comment">
                <a href="/" className="avatar">
                    <img alt="avatar" src={faker.image.avatar()} />
                </a>
                <div className="content">
                    <a href="/" className="author">{faker.name.firstName()}</a>
                    <div className="metadata">
                        <span className="date">{faker.time.recent()}</span>
                    </div>
                    <div className="text">Nice Blog Post! {faker.lorem.text()}</div>
                </div>
            </div>
            {/* second comments */}
      {/* <div className="comment">
                <a href="/" className="avatar">
                    <img alt="avatar" src={faker.image.avatar()} />
                </a>
                <div className="content">
                    <a href="/" className="author">{faker.name.firstName()}</a>
                    <div className="metadata">
                        <span className="date">{faker.time.recent()}</span>
                    </div>
                    <div className="text">Nice Blog Post! {faker.lorem.text()}</div>
                </div>
            </div>
            third Comment */}

      {/* <div className="comment">
                <a href="/" className="avatar">
                    <img alt="avatar" src={faker.image.avatar()} />
                </a>
                <div className="content">
                    <a href="/" className="author">{faker.name.firstName()}</a>
                    <div className="metadata">
                        <span className="date">{faker.time.recent()}</span>
                    </div>
                    <div className="text">Nice Blog Post! {faker.lorem.text()}</div>
                </div>
            </div> */}
    </div>
  );
};

// rendering reactdom component to show it on screen
ReactDOM.render(<App />, document.querySelector("#root"));
