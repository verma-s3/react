// importing react libraries 
import React from 'react';

// no need to import fakerlibrary here as we are not using it here anymore.
//import faker from 'faker';

// Creating componentdetail function 
// function name same as a file name 
const CommentDetail = (props)=> {
    return (
        <div className="comment">
            <a href="/" className="avatar">
                <img alt="avatar" src={props.image} />
            </a>
            <div className="content">
                <a href="/" className="author">{props.author}</a>
                <div className="metadata">
                    <span className="date">{props.timedate}</span>
                </div>
                <div className="text">Nice Blog Post! {props.text}</div>
            </div>
            
        </div>
    );
}

// fro component nesting
export default CommentDetail;