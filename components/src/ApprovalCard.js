// this is approval card with two buttons reject and accept

import React from "react";

// creating ApprovalCard function hereS

const ApprovalCard = (props) => {
  console.log(props.children);
  return (
    <div className="ui card">
      <div className="content">{props.children}</div>
      <div className="extra content">
        <div className="ui two buttons">
          <div className="ui basic green button">Approve</div>
          <div className="ui basic red button">Reject</div>
        </div>
      </div>
    </div>
  );
};

// for exporting the file into another file

export default ApprovalCard;
