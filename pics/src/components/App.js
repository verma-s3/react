import React from 'react';
import SearchBar from './SearchBar';


const App = ()=>{
    return(
        <div className='ui container' style={{marginTop: '50px'}} ><SearchBar /></div>
    );
}

export default App;