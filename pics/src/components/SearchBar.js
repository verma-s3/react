import React from 'react';

class SearchBar extends React.Component{
    // onInputChange(event){
    //     console.log(event.target.value);
    // }

    // onInputClick(){
    //     console.log("input was clicked");
    // }
    state = {term: ""};

    onFormSubmit = (event) =>{
        event.preventDefaiult();
        console.log(this.state.term);
    }

    render(){
        return(
            <div className="ui segment">
                <form onSubmit={this.onFormSubmit} className="ui form">
                    <div className ="field">
                        <label>Image Search</label>
                        {/* uncontrolled ways */}
                        {/* <input type="text" name="seacrh" onClick={this.onInputClick} onChange={this.onInputChange} /> */}
                        {/* <input type="text" name="seacrh" onChange={(event) => console.log(event.target.value)} /> */}
                        {/* controlled ways */}
                        <input type="text" name="seacrh" value={this.state.term} onChange={(event) => this.setState({term: event.target.value})} />
                        {/* Controlled components with make user to forcing type in uppercase */}
                        {/* <input type="text" name="seacrh" value={this.state.term} onChange={(event) => this.setState({term: event.target.value.toUpperCase()})} /> */}
                    </div>
                </form>
            </div>
        ) ;
    }
}

export default SearchBar;