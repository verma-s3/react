import React from 'react';

//new function to get lat and month for determining it is summer or winter 
const getSeason = (lat, month) => {
    if(month>2 && month<9){
        return lat > 0 ? 'summer' : 'winter' ;
    }else{
        return lat > 0 ? "winter" : "summer" ;
    }
}

// main season function to be used in index.js
const SeasonDisplay = (props) => {
    // console.log(props.lat);
    const season = getSeason(props.lat, new Date().getMonth());
    // console.log(season);
    // return <div>Season Display</div>;
    return (
        <div>{season === 'winter' ? "Burr! it is chilly" : "let's hit the beach" }</div>
    );
};

export default SeasonDisplay;
