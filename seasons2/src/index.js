import React from 'react'; 
import { createRoot } from 'react-dom/client';
import SeasonDisplay from './SeasonDisplay';


if (module.hot) {
    module.hot.accept();
  }
//import App from 'App';

// const App= ()=> {
//     window.navigator.geolocation.getCurrentPosition(
//         position => console.log(position),
//         err => console.log(err)
//     );
//     return <div>Hi there!</div>;
// };

class App extends React.Component {

    //first way of initialising state under the constructor method
    // constructor(props){
        // super(props);
        // {} is a state object 

        // THIS IS THE ONLY TIME WE DO DIRECT ASSIGNMENT TO this.state
        // this.state = { lat: null, errorMessage: "" };

        // window.navigator.geolocation.getCurrentPosition(
        //     //position => console.log(position),
        //     (position) => {
        //         // we called setstate !!!!!
        //         this.setState({ lat: position.coords.latitude});
        //         // wedid not
        //     },
        //     //err => console.log(err)
        //     (err) => {
        //         this.setState({errorMessage: err.message});
        //     }
        // );
        
    // }
    //second way of initialising state
    state = { lat: null, errorMessage: "" };
    //adding geolocation function in componentDIdMount
    componentDidMount(){
        window.navigator.geolocation.getCurrentPosition(
            (position) => this.setState({ lat: position.coords.latitude}),
            (err) => this.setState({errorMessage: err.message})
        ); 
    }
    // ReactJs Components
    // componentDidMount(){
    //     console.log('My component was rendered to the screen.');
    // }
    // componentDidUpdate(){
    //     console.log('My component was just updated - it rendered.');
    // }
    render() {
       

        // return <div>latitude: {this.state.lat} <br />
        //             Error: {this.state.errorMessage}    
        //         </div>;
        if(this.state.errorMessage && !this.state.lat){
            return <div>Error: {this.state.errorMessage}</div>
        }

        if (!this.state.errorMessage && this.state.lat){
            // return <div>latitude: {this.state.lat} </div>
            return <SeasonDisplay lat={this.state.lat} />
        }

        return <div>loading!!!</div>;
    }
}

const container = document.querySelector('#root');
const root = createRoot(container);

root.render(<App />)  